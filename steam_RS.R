### R script
## ====================================================


### require packages
## ====================================================

library(rjson)
library(RCurl)
library(parallel)
library(XML)
library(proxy)
library(igraph)
library(ggplot2)
library(ggdendro)


### all games information(with games steamid and name)
## ====================================================

u = "http://api.steampowered.com/ISteamApps/GetAppList/v0001/"
web = getURLContent(u)
value = fromJSON(web)
Aappid = sapply(value$applist$apps$app, function(x) x$appid)
Aname = sapply(value$applist$apps$app, function(x) x$name)
## ====================================================



### informal getPrice
## ====================================================

getPrice <- function(appid){
  
  u = paste0("http://store.steampowered.com/app/", appid, "/")
  doc = htmlParse(u)
  price = xpathSApply(doc,
                      "//div[@class = 'game_purchase_price price']",
                      xmlValue)
  if (length(price) == 0) return(0)
  ans = min(as.numeric(gsub("[\r|\n|\t|$]", "", price)))
  ans
}
## ====================================================



### informal getGenre
## ====================================================

getGenre <- function(appid){
  
  u = paste0("http://store.steampowered.com/app/", appid, "/")
  doc = htmlParse(u)
  t = getNodeSet(doc, "//div[@class = 'block game_details underlined_links']//b/following-sibling::a")
  xmlValue(t[[1]])
}
## ====================================================



### getAppinformation
## ====================================================

getApp <- function(id){
  
  u = paste0("http://store.steampowered.com/api/appdetails?appids=", 
             id, "&cc=us")
  tr = tryCatch(getURLContent(u), finally = TRUE,HTTPError = function(e){NULL})
  if (length(tr) == 0) return()
  web = getURLContent(u)
  if (web == "")   return(list(appid = id, genre = NULL, price = data.frame(packname = "", price = NA)))
  value = fromJSON(web, unexpected.escape = "keep")
  tryCatch(fromJSON(web, unexpected.escape = "keep"), HTTPError = function(e){NULL})
  if (!value[[1]]$success)   return(list(appid = id, genre = NULL, price = data.frame(packname = "", price = NA)))
  appid = id
  name = value[[1]]$data$name
  genre = value[[1]]$data$genres[[1]]$description
  if (length(value[[1]]$data$package_groups) == 0)
    return(list(appid = id, genre = genre, price = data.frame(packname = "", price = NA)))
  aboutP = sapply(value[[1]]$data$package_groups[[1]]$subs, function(pack){
    pack$option_text
  })
  packname = sapply(aboutP, function(x) strsplit(x, " - ")[[1]][1])
  packprice = sapply(aboutP, function(x) strsplit(x, " \\$")[[1]][2])
  price = data.frame(packname = packname, price = as.numeric(packprice))
  list(appid = appid, genre = genre, price = price)
}
## ====================================================



### Getfriend API
## ====================================================

getfriendId <- function(id){

key = "5E0E10CB0F03A5A8C68EDDC71A370A55"
u = paste0("http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key="
           , key, "&steamid=", id, "&relationship=friend")

tr = tryCatch(getURLContent(u), finally = TRUE,HTTPError = function(e){NULL})
if (length(tr) == 0) return()
web = getURLContent(u)
value = fromJSON(web)

step = sapply(value$friendslist$friends, function(x) x$steamid)
step
}
## ====================================================



### getOwnedgame API
## ====================================================

getOwnedgame <- function(id){
  
  key = "5E0E10CB0F03A5A8C68EDDC71A370A55"
  u = paste0("http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key="
             , key, "&steamid=", id, "&format=json&include_appinfo=1")
  
  tr = tryCatch(getURLContent(u), HTTPError = function(e){NULL})
  if (length(tr) == 0) return()
  web = getURLContent(u)
  value = fromJSON(web)
  
  games = sapply(value$response$games, function(x) {
                  tt = as.integer(x$playtime_forever)
                  structure(tt, names = x$name)
        })
  games
}
## ====================================================



### bfs to build model
## ====================================================

bfs <- function(id = id){
  
#initialize
i = 1
j = 1
m1 = character(1000)
m2 = character(1000)
m2[1] = id

# Use while loop to control the queue and the maximum length is 1000.
while (i <= j && i <= 1000){
  
  id = m2[i]
  if (j > 1000) break
  child = getfriendId(id)
  
  # Add child nodes into queue
  if (length(child) > 0 && j+length(child) < 1000) {
    m1[(j+1):(j+length(child)+1)] = rep(id, length(child))
    m2[(j+1):(j+length(child)+1)] = child
    j = j+length(child)
   }
  
  # Move to next node
  i = i+1
 }

# Combine two nodes together, this will be relation information
cbind(m1 = m1, m2 = m2)
}
## ====================================================



### parallel to create graph matrix
## ====================================================
f = c("76561197960435530", "76561197960434622", "76561197972495328", "76561197960534622", "76561197987069371")
cl = makeCluster(5, "FORK")

tt = clusterApply(cl, f, function(x){
    bfs(x)
})

mall = do.call(rbind, tt[-1])
del = which(mall[,1] == "")
m1 = mall[-del, 1]
m2 = mall[-del, 2]
m = union(m1,m2)
p1 = match(m1, m)
p2 = match(m2, m)

p = matrix(rep(0, length(m)*length(m)), length(m))
p[cbind(p1, p2)] = 1
p[cbind(p2, p1)] = 1
 
m1 = m$m1[-1]
m2 = m$m2[-1]
m = union(m1,m2)
p1 = match(m1, m)
p2 = match(m2, m)

p = matrix(rep(0, length(m)*length(m)), length(m))
p[cbind(p1, p2)] = 1
p[cbind(p2, p1)] = 1
## ====================================================



### using igraph to plot
## ====================================================

x = graph.adjacency(p, mode = c("undirected"))
l = layout.kamada.kawai(x)
plot(x, layout = layout.kamada.kawai, vertex.color = grouped,
     vertex.size = 1, vertex.label = NA)
## ====================================================



### union all games
## ====================================================

games = sapply(m, getOwnedgame)
save(games, file = "data2.rda")

allname = lapply(games, function(x){
        names(x)
})

allgames = unique(unlist(structure(allname, names = NULL)))

allgameid = Aappid[match(allgames, Aname)]
## ====================================================



### method 1
## ====================================================
mt = matrix(rep(0, length(games)*length(allgames)), length(games))

sapply(1:length(games), function(x){
  p = games[[x]];
  k = match(names(p), allgames);
  if (length(k)>0){
    kk = cbind(rep(x, length(k)), k);
    mt[kk] <<- 1
  }
})
## ====================================================



### hierarchy cluster method 1
## ====================================================
diss = dist(data.matrix(mt), method = "cosine")

h <- hclust(diss, method = "ward.D2")
plot(h, cex = .1, hang = 0.01, main = "Method 1 Cluster Dendrogram", xlab = "All games given")
grouped = cutree(h, 4)

o = split(1:length(grouped), grouped)
ord = order(apply(mt[o[[1]], ], 2, mean))[1:3]
allgames[ord]
## ====================================================



### igraph method 1
## ====================================================
x = graph.adjacency(p, mode = c("undirected"))
h1 <- hclust(diss, method = "ward.D2")
grouped = cutree(h1, 4)
plot(x, layout = l, vertex.color = grouped + 1,
     vertex.size = 2, vertex.label = NA, 
     main = "Method 1 Cluster(k = 4)")
## ====================================================

### method 2
## ====================================================
mt = matrix(rep(0, length(games)*length(allgames)), length(games))

sapply(1:length(games), function(x){
  p = games[[x]];
  k = match(names(p), allgames);
  if (length(k)>0){
    kk = cbind(rep(x, length(k)), k);
    mt[kk] <<- p
  }
})

mt = as.data.frame(mt)
pc = prcomp(mt)
mt = pc$x[, 1:100]

# match.mt <- function(x, allgames){
#   mt = rep(0, length(allgames))
#   p = game[[x]];
#   k = match(p, allgames);
#   mt[k] = 1
#   mt
# }
# 
# p = sapply(1:length(m), function(x) match.mt(x, allgames))
## ====================================================



### hierarchy cluster method 2 
## ====================================================
diss = dist(data.matrix(mt), method = "cosine")

h <- hclust(diss, method = "ward.D2")
plot(h, cex = .1, hang = 0.01, main = "Method 2 Cluster Dendrogram", xlab = "All games given")
grouped = cutree(h, 4)

o = split(1:length(grouped), grouped)
ord = order(apply(mt[o[[1]], ], 2, mean))[1:3]
allgames[ord]
## ====================================================



### igraph method 2
## ====================================================
x = graph.adjacency(p, mode = c("undirected"))
h1 <- hclust(diss, method = "ward.D2")
grouped = cutree(h1, 4)
plot(x, layout = l, vertex.color = grouped + 1,
     vertex.size = 2, vertex.label = NA, 
     main = "Method 2 Cluster(k = 4)")
## ====================================================



#genre cluster

allgenres = sapply(allgameid, getApp)
allgenre = unique(unlist(allgenres))
mg = matrix(rep(0, length(m)*length(allgenre)), length(m))

sapply(1:length(m), function(x){
  p = games[[x]];
  k = match(unique(unlist(allgenres[match(names(p), allgames)])), allgenre)
  if (length(k)>0){
    kk = cbind(rep(x, length(k)), k);
    mg[kk] <<- mg[kk] + 1
  }
})

dissmg = dist(mg, method = "cosine")
h <- hclust(diss, method = "ward.D2")
plot(h)
grouped = cutree(h, 5)
